# SNMP-Proxy
Golang-Service for caching and proxying snmp data.

Currently only proxying is implemented.


## Usage
### Running API
TODO
### Doc
TODO


## Ideesammlung
  * erst mal max_age == inf
  * versionierte API (/v1/...)
  * Parameter um einen Force-Reload trotz Cache zu machen
    * besser: ?max_age=n (0==reload, in seconds)
  * Parameter für Community-String (?community=XXXXXXX)
  * Api Struktur:
    * Schema .../$adresse/$oid
      * Separate .../walk und .../get funktionen
    * eine query, und via parameter (...?mode=walk|get) Walk und Get unterscheiden
  * Walk & Get so implementieren, das Walk gechachte Get Werte nicht erneut abgerufen werden
  * Ablage im Redis:
    * key: $adresse-$oid (nur Hostnames als adresse)
      * wie handled man IPv4 <-> IPv6?
        * netdoc? -> default v6 speichern, v4 only in v4 speichern
        * reverse-dns?
      * welche OID-Darstellung als Referenz?
    * value: wert
    * alles serialisieren
  * Auth:
    * Token?
      * LDAP als Quelle
      * -> HTTP Basic Auth
    * alternative: Kein Auth, aber nur bestimmt IPs dürfen zugreifen (ferm)
  * nur Hostnames akzeptieren, IPs abweisen -> Target auflösen
