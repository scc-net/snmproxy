package controller

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

// Index godoc
// @Summary Index handler
// @Description Root index handler
// @Produce plain
// @Success 200 {object} string
// @Router / [get]
func (c *Controller) Index(ctx *gin.Context) {
	ctx.String(http.StatusOK,	"SNMP Proxy API. Current in Beta Phase. Doc in progress, access it at /swagger/index.html.")
}