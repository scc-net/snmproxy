package controller

import (
	"github.com/gin-gonic/gin"
	"github.com/soniah/gosnmp"
	"net/http"
	"snmProxy/internal/httputil"
	"snmProxy/internal/snmp"
	"time"
)

// Get godoc
// @Summary Get SNMP Value
// @Description gets SNMP-Value for given host
// @Tags snmp
// @Produce json
// @Param host path string true "Host FQDN"
// @Param oid path string true "OID"
// @Success 200 {object} snmp.Data
// @Failure 500 {object} httputil.HTTPError
// @Router /get/{host}/{oid} [post]
func (c *Controller) Get(ctx *gin.Context)  {
	host := ctx.Param("host")
	oid := ctx.Param("oid")

	query := &gosnmp.GoSNMP{
		Target: host,
		Community: ctx.DefaultPostForm("community", "public"),
		Port: 161,
		Version: gosnmp.Version2c,
		Timeout: 2 * time.Second,
		Retries: 3,
	}

	if err := query.Connect(); err != nil {
		httputil.NewError(ctx, http.StatusInternalServerError, err)
	}
	defer snmp.CloseSNMPConn(query)

	oids := []string{oid}
	result, err := query.Get(oids)
	if err != nil {
		httputil.NewError(ctx, http.StatusInternalServerError, err)
	}

	j := snmp.SerializeSnmpPDU(result.Variables[0])
	ctx.JSON(http.StatusOK, j)
}

// Walk godoc
// @Summary Get SNMP Value
// @Description gets SNMP-Value for given host
// @Tags snmp
// @Produce json
// @Param host path string true "Host FQDN"
// @Param oid path string true "OID"
// @Success 200 {object} []snmp.Data
// @Failure 500 {object} httputil.HTTPError
// @Router /walk/{host}/{oid} [post]
func (c *Controller) Walk(ctx *gin.Context) {
	host := ctx.Param("host")
	oid := ctx.Param("oid")

	query := &gosnmp.GoSNMP{
		Target: host,
		Community: ctx.DefaultPostForm("community", "public"),
		Port: 161,
		Version: gosnmp.Version2c,
		Timeout: 10 * time.Second,
		Retries: 3,
	}

	if err := query.Connect(); err != nil {
		httputil.NewError(ctx, http.StatusInternalServerError, err)
	}
	defer snmp.CloseSNMPConn(query)

	result, err := query.BulkWalkAll(oid)
	if err != nil {
		httputil.NewError(ctx, http.StatusInternalServerError, err)
	}

	var list []*snmp.Data
	for _, element := range result {
		tmp := snmp.SerializeSnmpPDU(element)
		list = append(list, tmp)
	}
	ctx.JSON(http.StatusOK, list)
}