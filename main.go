package main

import (
	"context"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/mediocregopher/radix/v3"
	"github.com/swaggo/files"
	"github.com/swaggo/gin-swagger"
	"log"
	"net/http"
	"os"
	"os/signal"
	"snmProxy/controller"
	_ "snmProxy/docs"
	"syscall"
	"time"
)

func createRedisPool() *radix.Pool {
	pool, err := radix.NewPool("tcp", "127.0.0.1:6379", 10)
	if err != nil {
		fmt.Println(err)
	}
	return pool
}

// @title SNMProxy API
// @version 0.0.4
// @description SNMProxy REST API Server

// @contact.name SCC-NET
// @contact.email net-server@lists.kit.edu

// @license.name MIT
func main() {
	r := gin.Default()

	c := controller.NewController()

	r.GET("/", c.Index)
	r.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	r.POST("/get/:host/:oid", c.Get)
	r.POST("/walk/:host/:oid", c.Walk)

	srv := &http.Server{
		Addr:         ":8000",
		// Good practice to set timeouts to avoid Slowloris attacks.
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      r, // Pass our instance of gin in.
	}

	// Initializing the server in a goroutine so that
	// it won't block the graceful shutdown handling below
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Wait for interrupt signal to gracefully shutdown the server with
	// a timeout of 5 seconds.
	quit := make(chan os.Signal)
	// kill (no param) default send syscall.SIGTERM
	// kill -2 is syscall.SIGINT
	// kill -9 is syscall.SIGKILL but can't be catch, so don't need add it
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutting down server...")

	// The context is used to inform the server it has 5 seconds to finish
	// the request it is currently handling
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown:", err)
	}

	log.Println("Server exiting")
}
