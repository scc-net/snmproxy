package httputil

import "github.com/gin-gonic/gin"

// NewError creates an gin JSON Response containing status-code and error message
func NewError(ctx *gin.Context, status int, err error) {
	er := HTTPError{
		Code:    status,
		Message: err.Error(),
	}
	ctx.JSON(status, er)
}

// HTTPError Model definition
type HTTPError struct {
	Code    int    `json:"code" example:"500"`
	Message string `json:"message" example:"status bad request"`
}