package snmp

import (
	"github.com/soniah/gosnmp"
	"log"
	"strings"
)

// Data Struct for SNMP Data serialization
type Data struct {
	OID string
	Value interface{}
}

// SerializeSnmpPDU takes an gosnmp.SnmpPDU object as argument,
// serializes it into strings or ints and returns an Data struct.
func SerializeSnmpPDU(pdu gosnmp.SnmpPDU) *Data {
	serialized := &Data{}
	// Remove uncommon dot at the beginning
	serialized.OID = strings.TrimLeft(pdu.Name, ".")

	// Parse Value based on type
	switch pdu.Type {
	case gosnmp.OctetString:
		serialized.Value = string(pdu.Value.([]byte))
	case gosnmp.ObjectIdentifier:
		serialized.Value = strings.TrimLeft(pdu.Value.(string), ".")
	default:
		serialized.Value = gosnmp.ToBigInt(pdu.Value)
	}

	return serialized
}

// CloseSNMPConn closes an snmp-connection and catches the error if one occurs.
func CloseSNMPConn(conn *gosnmp.GoSNMP) {
	if err := conn.Conn.Close(); err != nil {
		log.Fatalf("Close() err: %v", err)
	}
}